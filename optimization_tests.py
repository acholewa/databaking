from __future__ import division
import unittest
import optimization

class TestOptimization(unittest.TestCase):

    def  setUp(self):
        #expect to be able to make 25 servings.
        self.num_servings_make = 50
        self.maximum_cost = 100
        self.num_servings_recipe = 5
        self.amount_each_ingredient_pantry = [30, 40, 50]
        self.amount_each_ingredient_recipe = [1, 2, 3]
        self.cost_each_ingredient = [3, 2, 5]

    def test_enough_ingredients(self):
        recipe_scale = 5
        result = optimization.enough_ingredients(recipe_scale, self.amount_each_ingredient_pantry, self.amount_each_ingredient_recipe)
        self.assertEqual(result, True)

    def test_optimization(self):
        num_servings = optimization.optimize_recipe(self.num_servings_make, self.maximum_cost, self.num_servings_recipe, 
                self.amount_each_ingredient_pantry, self.amount_each_ingredient_recipe, self.cost_each_ingredient)
        self.assertEqual(num_servings, 25)

optimizationSuite = unittest.TestLoader().loadTestsFromTestCase(TestOptimization)
unittest.TextTestRunner(verbosity=2).run(optimizationSuite)



