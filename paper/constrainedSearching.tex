A common limitation in other recipe databases (such as \url{allrecipes.com}) is that they have very limited notions of 
search. One may search by recipe name, recipe category, and perhaps by ingredients, but not much more. For example, 
the user may not be able to explicitly disallow some recipes, or put special emphasis on others. Similarly, the user
cannot focus on recipes that he/she can actually make with the ingredients he/she has on hand, or that will cost below
a certain amount of money to make.
However, the user Therefore, we introduce a notion of constrained searching based on pantries. 
Associated with each user is a pantry. The pantry stores the list of ingredients currently owned by the user. Then, the user
may search for classes of recipes that the user can make based on the contents his/her pantry, and several other 
criteria.

The criteria are:

\begin{itemize}

    \item \textbf{Category} - The type of recipe the user is interested in, i.e. cookies or cakes.
    \item \textbf{Desired Ingredients} - A comma separated list of ingredients that the user is particularly interested in.
    \item \textbf{Ingredients to Avoid} - A comma separated list of ingredients that the user 
    \item \textbf{Username} - The username of the pantry we would like to use.
    \item \textbf{Maximum Amount to Spend} - The maximum cost of the ingredients you're willing to use, i.e. \$100 worth of ingredients.
    \item \textbf{Number of servings to make} - The number of servings you'd like to make.
\end{itemize}

Fancy search will then find all recipes in \textbf{Category} (or all recipes, if  no category is provided) that the user can make \textbf{Number of Servings} servings for \textbf{Maximum amount}
dollars based on the current contents of the user's pantry. If all three of these criteria cannot be meant (say the user isn't willing to spend enough, or the user doesn't have enough ingredients
in their pantry to make \textbf{Number of Servings}) then the program will find those recipes that get as close as possible to \textbf{Number of Servings} while still remaining below the cost
and pantry thresholds. Similarly, the \textbf{Desired Ingredients} and \textbf{Ingredients to Avoid} further influence the search. A recipe is considered a ``better" match for every ingredient
in \textbf{Desired Ingredients} that the recipe uses, while considered a ``worse" match for every ingredient the recipe uses that is in \textbf{Ingredients to Avoid}.

\subsubsection{Weights}

The search is performed by assigning weights to each of the options that the user provides. The weight for each recipe $r$ is computed using the following formula:

\begin{align}
    \label{weightEquation}
    \begin{aligned}
        &a_1*(\mathit{numServings}) + a_2*(\mathit{numDesiredIngredients}) -\\
        &a_3*(\mathit{numUndesiredIngredients}) - \mathit{numServings}*(\mathit{numInsufficientIngredients}) -\\
        &a_4*(\mathit{recipeDifficulty})
    \end{aligned}
\end{align}

where \textit{numServings} is the maximum number of servings that can be made of recipe $r$ based on the contents of the pantry. The variable \textit{numDesiredIngredients} represents the 
number of desired ingredients that appear in the recipe, while \textit{numUndesiredIngredients} represents the number of undesired ingredients in the recipe. Finally, 
\textit{numInsufficientIngredients} is the number of ingredients that the recipe requires that the pantry does not have enough of. Note that category does not have an assigned weight to it.
When a category is provided, we obtain only those recipes associated with that category. So if the user types ``cookies" into the category section, then we will only investigate recipes for
cookies.

So, suppose our pantry contains 100 teaspoons of milk,
300 teaspoons of flour, and 400 teaspoons of sugar (all ingredients are stored internally as teaspoons). Furthermore, suppose we have a recipe ``milk cookies" that requires 20 teaspoons of milk,
50 teaspoons of flour, 10 teaspoons of sugar, and twelve teaspoons of coconut slivers. Suppose we would like to make 5 servings of some cookie or other, and we don't care how much it costs. 
Furthermore, suppose we don't like coconut, but we really like milk. So we want our recipe to contain milk, but not coconut. Furthermore, suppose the recipe has a difficulty of 1. Finally, suppose 
our weights are
\begin{align*}
    a_1 &= 10\\
    a_2 &= 5\\
    a_3 &= 5\\
    a_4 &= 3
\end{align*}
Then, the weight for ``milk cookies" would be:

\begin{align*}
    10(0) + 5(1) - 5(1) - (5)(1) - 3(1) = -8
\end{align*}

Note that we can't make any servings because we don't have any coconut slivers.

Meanwhile, suppose we have a recipe ``super milk cookies" that has as ingredients 20 teaspoons of milk, 50 teaspoons of flour, and 10 teaspoons of sugar with a difficulty of 1. Then our weight is:

\begin{align*}
    10(5) + 5(1) - 5(0) - 5(0) - 3(1) = 52
\end{align*}

So ``super milk cookies" will show up much higher on the results list than ``milk cookies." Furthermore, those recipes that the user can actually make will show up at the top of the results list,
while those that he/she can almost make will show up in the middle, and those he/she cannot make at all will show up at the bottom.

\subsubsection{Maximizing the Weight}

So mathematically speaking, we wish to find those recipes that maximize the value of equation~\ref{weightEquation}. At first, this looks like a linear programming problem: we would like to maximize
a linear function on competing resources. Therefore, we first began to investigate algorithms for linear programming, and we determined the following linear programming problem:

\begin{align*}
    \mathrm{maximize:}\\
    a_1x_1 + \sum_{i=1}^n y_1 + \sum_{i=1}^mz_i + a_4x_4\\
    \mathrm{where}\\
    \begin{aligned}
        x_1 &- \mathrm{number~of~servings}\\
        y_i &- \mathrm{weight~associated~with~desired~ingredient~i}\\
        z_i &- \mathrm{negative~weight~associated~with~undesired~ingredients~i}\\
        x_4 &- \mathrm{negative~difficulty}\\
        c   &- \mathrm{maximum~cost}\\
    \end{aligned}\\
    \mathrm{such~that}\\
    \frac{x_1}{\mathit{numServings}} \sum\limits_{i \in \mathit{Ingredients}} cost(i) * \mathit{numTeaspoons}(i) \le c\\
    \mathrm{and}\\
    y_i(\mathit{numTeaspoons}(i)) \le \mathit{pantry}(i)~\mathit{for~} i \in \mathit{recipeIngredients}
\end{align*}

where \textit{numServings} is the number of servings that the recipe makes.

To solve this problem, we began to implement the simplex algorithm, the most popular and  often (in practice) most efficient algorithm for solving linear programming problems.

However, while working on an implementation of the simplex algorithm, we had a key insight. Observe that all of the parameters above except for the number of servings to make, are set in stone
as soon as a recipe is chosen. One cannot dynamically change the ingredients in a recipe, or the difficulty, or the cost. Furthermore, because SQL is such a simple language, we have to pull every
recipe in the desired category into memory before we can work with it anyway. Therefore, we can simplify the above problem to the following:

\begin{align*}
    \mathrm{For~each~recipe:}\\
    \mathrm{maximize:}\\
    a_1x_1\\
    \mathrm{such~that}\\
    \frac{x_1}{\mathit{numServings}} \sum\limits_{i \in \mathit{Ingredients}} cost(i) * \mathit{numTeaspoons}(i) \le c\\
    \mathrm{and}\\
    y_i * (\mathit{numTeaspoons}(i)) \le \mathit{pantry}(i)~\mathit{for~} i \in \mathit{recipeIngredients}
\end{align*}

To maximize $a_1x_1$ we only need to perform a simple binary search on the number of servings to make. Let $s$ be the number of servings the user wants to make, and $c$ the amount of money he/she is 
willing to spend on ingredients.
Then, for each recipe $r$, we consider the range $[0, s]$. We compute the cost of making $s / 2$ servings. If the cost is above $c$, or we do not have enough ingredients to make $s/2$ servings, then 
we consider the range 
$[0, s/2)$. If the cost is below $c$ and we have enough ingredients to make $s / 2$ servings, we consider
    the range $[s/2, s]$. By recursing, we can then compute the maximum number of servings that can be made of recipe $r$ with pantry $p$ that does not exceed cost $c$.

Once we have the maximum number of servings we can make, we plug it into $x_1$ in equation~\ref{weightEquation}, along with the rest of the information (which is stored in the database already)
to compute the weight for that recipe. Currently, the weights are hard-coded. Future iterations of the database may allow the user to specify the weights for each section. So the user would be
able to tell us how important it is that he/she can actaully make 100 servings, or how important it is that we avoid the ingredient ``milk" or include the ingredient ``chocolate chips."

\subsubsection{Obtaining the Needed Information}

To obtain the information we need to compute the weights for the recipes, we need to touch every part of the database. We need:

\begin{itemize}

    \item The recipes
    \item The ingredients used by each recipe
    \item The contents of the user's pantry
\end{itemize}

To obtain that information, we need to use several SQL queries. The first gets us all of the recipe information along with their associated ingredients:

\begin{verbatim}

SELECT *
FROM ((recipe 
    NATURAL JOIN 
    uses) 
    NATURAL JOIN
    ingredient)
WHERE category = <userProvidedCategory>
\end{verbatim}

Note that the \verb|WHERE| clause is not included if the user does not provide a category to extract the recipes from. This query gives us a table that merges the recipes with the ingredients that 
they use. Note that the \verb|recipe| and \verb|uses| tables share only the \verb|recipe_id| attribute. Therefore, \verb|recipe NATURAL JOIN uses| will combine recipes with the ingredients used
by those recipes. Similarly, this new table only shares the \verb|ngredient_name| attribute with the ingredient table. Therefore, when the new table is joined with ingredient, it will have the 
effect of combining the recipes and associated ingredients, with the costs of each ingredient.

Our second query obtains for us the user's pantry:

\begin{verbatim}

SELECT ingredient_name, quantity
FROM (pantry
     NATURAL JOIN contains)
WHERE username = <userProvidedUserName>
\end{verbatim}

This command combines the desired username with the ingredients associated with that username (since the only attribute shared by \verb|pantry| and \verb|contains| is the username). Essentially,
this gives us a table of the ingredients contained in the current user's pantry.

From here, it's a simple matter of putting the information into a format that can be easily used for computing the number of servings we can make for each recipe, and once we have that, the 
associated weight of each recipe.
