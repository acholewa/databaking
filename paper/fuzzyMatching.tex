Our second advanced functionality is approximate search of recipe names. The entire point of a database like this is to
allow the user to find useful information using vague or inaccurate information that they already ``know" or can guess. 
Therefore, if we required the user to know the exact name (or an exact substring) of a recipe, then the database would
be at best frustrating, at worst useless. Therefore, if we want our database to be anything approximating usable, we 
\emph{must} have some form of approximate matching, at the very for our basic search functionality.

So the problem we would like to solve is the following: Given a query by the user that may be vague, short, and even
ridden with typos, we would like to guess what the user intended, and present recipes whose titles most closely match
our guess. Needless to say, this is much more challenging than simply blindly matching on exact equality in a SQL
select statement. It also requires much more power than what is provided by a SQL LIKE statement; much of the time 
the user's input won't match any sane pattern. It will only approximately match a pattern!

\subsubsection{What does it mean to guess?}

The most challenging aspect of this problem is defining precisely what it means to guess, or approximately match two
strings. As a result, we tried a variety of different approaches, for example longest common substrings, or longest common
subsequences. The most promising approach was a notion of ``edit distance," a well-known concept in computer science. 
Edit distance is a way of measuring the dissimilarity between two strings by counting how many edit operations are needed
to make the strings the same. The edit operations are:

\begin{itemize}

\item delete a character from one string
\item insert a character into one string
\item replace a character in one string with a different character (usually from the other string)

\end{itemize}

So for example, ``cat" and ``car" have an edit distance of one: by replacing the ``t" in ``cat" with ``r", we change
``cat" into ``car." 

At first, this held promise. However, after implementing the Wagner-Fischer algorithm, we discovered a problem. Computing 
the edit 
distance of the two strings directly puts disproportionate emphasis on the lengths of the strings. For
example, if the user input a search term ``cookie," very short recipe names, such as ``irish tea cake" that had nothing to 
do with cookies were
favored over longer recipe names, such as ``best big, fat, chewy chocolate chip cookie", that may contain ``cookie" as a 
substring. The primary reason for this is that the search term ``cookie" is not being compared against
``cookie" in the title. Rather, the first 6 characters in ``best big, fat, chewy chocolate chip cookie" are compared 
against ``cookie," replaced, and then all the other characters dropped. Since the exact same thing happens with
``irish tea cake," ``irish tea cake" will have a shorter edit distance, since it needs to delete fewer characters. 
However, conceptually what you would expect, is that the database would recognize the existence of cookie in 
``best big, fat, chewy chocolate chip cookie" and favor it over ``irish tea cake."

Therefore, we needed something more precise. After a bit of research, we hit upon the idea of comparing substrings. 
In other words, two strings are a close approximately match if one of the strings contains at least one substring with a 
very small edit distance to the other. 

\subsubsection{The Algorithm}

There are several approaches to computing approximate string matching that attempt to be ``substring aware." After some
thought, and research, we decided to settle on a rather brute force approach: let $s$ be the search term, and $t$ be the 
recipe title.
Then we split $t$ into all possible substrings, and compare each one against $s$ using the Wagner-Fischer algorithm
we implemented in a previous iteration of our feature. This simplified implementation tremendously (maximizing the 
chances that we would be able to implement it correctly), and it also allowed us to leverage unchanged our implementation
of Wagner-Fischer, which again saved us valuable time.

There are more efficient ways of performing approximate string matching, we only require a reasonably efficient approximate
string matching algorithm, because approximate string matching is not our bottleneck.
Even the longest of titles are not particularly long. 
The title ``best big, fat, chewy chocolate chip cookie" is one of the longest in the database, and it is only 42 characters
long. Instead, our bottleneck is the number of recipes we need to scan through. Therefore, if we wish to improve performance,
we should focus on reducing the number of recipes that we feed into our approximate string matching algorithm.

Therefore, we implemented an optimization based on the following heuristic: most of the time, the recipes of most interest
to the user are those that contain at least one of the user's search terms as a substring. Therefore, at the beginning
we split the user's search query into separate words, and generate a SQL command that uses the LIKE command to extract
only those recipes whose names contain at least one of the search terms.

For example, suppose the user input ``chocolate cookies" into the search box. Then, we generated the following SQL
query:

\begin{verbatim}
    SELECT name, recipe_id
    FROM recipe
    WHERE
    name LIKE '%chocolate%' OR
    name LIKE '%cookies%';
\end{verbatim}

Now, we will only approximately match those recipes that contain at least one of the search terms in the title to begin
with, allowing us to throw out the obviously useless recipes, like ``key lime pie." However, it may be the case that 
we don't receive any results (for example, the user inputs ``cokie" instead of ``cookie"). In that case, we compare against
all the recipes in the database. This allows us to leverage the years of heavy optimization built into the implementation 
of SQL queries in Django, providing us reasonable performance without having to spend even more hours heavily optimizing
our own code.

