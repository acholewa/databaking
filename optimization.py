from __future__ import division
"""
We have a few levels:
    1. Obtain all recipes in the desired recipe category.
    2. Narrow all such recipes to recipes that contain all ingredients with a weight of infinity (i.e. has to be there), and do not contain all ingredients to avoid with a weight of infinity 
    (i.e. absolutely cannot be there). Note that if a recipe has a substitution that adds an ingredient that has to be there, then we should also consider that new recipe. If a recipe has a 
    substitution that removes an ingredient that absolutely shouldn't be there then we include it.
    3. For each recipe:
        For each substitution:
            If the substitution is in Y, and the old ingredient is not in Y:
                replace the recipe with the new recipe where we include the substitution     
            elif the substitution is not in Z and the old ingredient is in Z:
                replace the recipe with the new recipe where we include the substitution
            elif the new is not in Y and the old is:
                ignore substitution
            elif the new is not in Z and the old is:
                ignore substitution
            otherwise:        
                generate two recipes: one with and one without the substitution
    4. For each recipe:
        value = a_1x_1 + Sigma_{i in use_ingredients union ingredients} a_2 - Sigma_{i in avoid_ingredients intersect ingredients} a_3  - a_4x_4
    return the 10 highest values.
"""


def optimize_recipe(num_servings_make, maximum_cost, num_servings_recipe, amount_each_ingredient_pantry, amount_each_ingredient_recipe, 
        cost_each_ingredient):
    """
    This function maximizes the number of servings. Let R be the set of ingredients in the current recipe, and let P be the set of ingredients in the current
    pantry. Then, Given the following arguments:
    1. num_servings_make - how many servings the user would like to make.
    2. maximum_cost - how much the user is willing to spend
    3. num_servings_recipe - how many servings the recipe makes
    4. amount_each_ingredient_pantry - number of units of measurement of each ingredient in R intersect P
    5. amount_each_ingredient_recipe - number of units of measurement of each ingredient in R
    6. cost_each_ingredient - the cost of each ingredient in R intersect P

    Returns the maximum number of servings we can make (up to num_servings_make) within our costs using the ingredients currently in our 
    pantry.

    I was going to use linear programming, but that algorithm's complicated as shit. Since I'm only optimizing number of servings, and I 
    have an upper bound (num_servings_make), I'm just going to brute force it.
    pre: len(amount_each_ingredient_recipe) == len(amount_each_ingredient_pantry) == len(cost_each_ingredient)
    post: The number of servings returned is less than or equal to num_servings_make
    """
    assert len(amount_each_ingredient_recipe) == len(amount_each_ingredient_pantry) == len(cost_each_ingredient),\
        "One of the lists are not the correct length Lengths: %s, %s, %s" %\
        (str(len(amount_each_ingredient_recipe)), str(len(amount_each_ingredient_pantry)), str(len(cost_each_ingredient)))
    baseRecipeCost = sum([ingredient_cost * amount_ingredient for ingredient_cost, amount_ingredient in zip(cost_each_ingredient, 
        amount_each_ingredient_recipe)])
    max_servings_make = num_servings_make
    servings = num_servings_make // 2
    if baseRecipeCost > maximum_cost or not enough_ingredients(1, amount_each_ingredient_recipe, amount_each_ingredient_pantry):
        return 0
    while True:
        recipe_scale = (servings // num_servings_recipe)
        cost =  recipe_scale * baseRecipeCost
        sufficient_pantry = enough_ingredients(recipe_scale, amount_each_ingredient_pantry, amount_each_ingredient_recipe)
        if cost == maximum_cost and sufficient_pantry:
            return servings
        elif cost > maximum_cost or not sufficient_pantry:
            max_servings_make = servings
            servings = max_servings_make // 2
        elif cost < maximum_cost and sufficient_pantry:
            max_servings_make, servings = (servings, servings + (max_servings_make - servings) // 2)
            if servings == max_servings_make:
                return servings


def enough_ingredients(recipe_scale, amount_each_ingredient_pantry, amount_each_ingredient_recipe):
    """
    Given the arguments:
    1. recipe_scale - By how much we're scaling up the recipe (i.e. 2, 3, ... times)
    2. amount_each_ingredient_pantry - The amount of each ingredient in the recipe that we have in the pantry
    3. amount_each_ingredient_recipe - The amount of each ingredient that the recipe requires.

    Returns true if there are enough of every recipe in the pantry, returns false otherwise.
    """
