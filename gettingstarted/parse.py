from __future__ import print_function
import backend.backend

def request_data(data):
	#data.name
	recipe = get_recipe(data)
	return backend.backend.get_data(recipe)

def search(model, request):
    #SELECT * FROM recipe WHERE name = 'chocolate chip cookies';
    q = request.GET['q']
    query = ' '.join(['SELECT recipe_id FROM search_recipe WHERE name=', "'" + q + "'"])
    recipes = Recipe.objects.raw(query)
    return recipes
	
def delete_recipe(recipe_name):
	return ' '.join(["DELETE * FROM recipe WHERE name =", recipe_name, ";"])

def insert_recipe(data):
	#data is what Kathryn sends in
	datastring = ','.join([data.id, data.cooktime, data.picture, data, difficulty, data.numofservings, data.link, data.directions, data.category, data.name, data.rating])
	return ' '.join(["INSERT INTO recipe (id, cooktime, picture, difficulty, numofservings, link, durections, category, name, rating)"
	"VALUES", datastring, ";"])

if __name__ == '__main__':
	print (request_data('chocolate chip cookie'))
