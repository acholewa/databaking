from __future__ import print_function
from backend import optimization
from django.db import connection
from django.db.models import Max
from django.db import IntegrityError
from operator import itemgetter
import models
import difflib
import warnings

MIN_LENGTH = 100
try:
	from .StringMatcher import StringMatcher as SequenceMatcher
except ImportError:
	warnings.warn('Using slow pure-python SequenceMatcher. Install python-Levenshtein to remove this warning')
	from difflib import SequenceMatcher

def search(model, request):
	#iterate through list and use fuzzy search to compare. and stuff
	q = request.GET['q']
        search_words = q.split()
        patterns = []
        #Since recipe names are so short, the bottleneck is not the means by which we compute the edit distance, but
        #rather the number of recipes we need to look at. Therefore,
        #we optimize the search a little bit, by only grabbing those recipes that contain at least one of the
        #search words somewhere in the name.
        for word in search_words:
            #Escape single quotes
            word = word.replace("'", "''")
            patterns.append(' '.join(["name LIKE", "'%%" + word.lower() + "%%'"]))
        like_patterns = ' OR '.join(patterns)
        selection_command = ' '.join(["SELECT name, recipe_id FROM", "search_" + model.__name__.lower()])
	getq = ' '.join([selection_command, "WHERE", "(", like_patterns, ")", ";"])
        #getq = ' '.join([selection_command, ";"])
	get_result = model.objects.raw(getq)
        results = get_result
        results = [result for result in get_result]
        if not results:
            #If we don't have anything then we grab every recipe.
            getq = ' '.join([selection_command, ";"])
	    get_result = model.objects.raw(getq)
            results = get_result
        best_matches = []
        names = []
        seen_matches = []
	for recipe in results:
            if recipe.name:
                current_match = fuzzy_string_match(recipe.name, q)
                seen_matches.append((recipe.name, current_match))
                if len(best_matches) < 10:
                    best_matches.append((current_match, recipe))
                else:
                    #remove worst match
                    worst_match, worst_match_recipe = max(best_matches, key=itemgetter(0))
                    if current_match < worst_match:
                        best_matches.remove((worst_match, worst_match_recipe))
                        best_matches.append((current_match, recipe))
	sorted_recipe_key_pairs = sorted(best_matches, key=itemgetter(0))
	return [recipe_key_pair[1] for recipe_key_pair in sorted_recipe_key_pairs]
	

def get_ingredients(recipe):
    """
    Given a recipe object, obtains all of the ingredients used by that recipe.
    """
    query = ' '.join(["SELECT uses_id, ingredient_name, quantity, unit FROM search_uses WHERE recipe_id = ", 
        recipe.recipe_id, ';'])
    return models.Uses.objects.raw(query)
"""
Using longest common substring algorithm to fuzzy match strings.
http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring
"""

DELETE_WEIGHT = 1
INSERT_WEIGHT = 1
REPLACE_WEIGHT = 1
def fuzzy_string_match(name, search_term):
    """
    Performs fuzzy string matching.

    Note: string_edit_distance does not account for longer names. Intuitively, sugar frosted caramel cookies to "cookies" 
    should be a 
    closer match than lime pie. However, because the former is so much longer than the latter, it is cheaper to just
    fully replace all the characters in lime pie to get cookies than it is to convert cookies into the former. 

    So we need to look at substrings.
    """
    min_edit_distance = float('inf')
    for substring in all_substrings(name):
        if substring:
            min_edit_distance = min(min_edit_distance, string_edit_distance(substring, search_term))
    return min_edit_distance

def all_substrings(name):
    already_seen = set()
    for start in range(len(name)):
        suffix = name[start:]
        for end in range(start, start + len(suffix)):
            substring = suffix[:end]
            if not substring in already_seen:
                already_seen.add(substring)
                yield substring




def string_edit_distance(name, search_term):
    """
    Given the name of a recipe, and the search term provided by the user, computes the edit distance between that
    search string and the user's search term.
    We compute the edit distance using the Wagner-Fischer algorithm.

    """
    edit_distances = []
    for i in range(len(search_term)):
        edit_distances.append([])
        for j in range(len(name)):
            edit_distances[-1].append(None) 
    for search_char in range(len(search_term)):
        for name_char in range(len(name)):
            if search_char == 0:
                edit_distances[search_char][name_char] = DELETE_WEIGHT * len(name[:name_char])
            elif name_char == 0:
                edit_distances[search_char][name_char] = DELETE_WEIGHT * len(search_term[:search_char])
            else:
                if search_term[search_char] == name[name_char]:
                    edit_distances[search_char][name_char] = edit_distances[search_char - 1][name_char - 1]
                else:
                    edit_distances[search_char][name_char] = min([
                            edit_distances[search_char - 1][name_char] + DELETE_WEIGHT, 
                            edit_distances[search_char][name_char-1] + INSERT_WEIGHT, 
                            edit_distances[search_char - 1][name_char - 1] + REPLACE_WEIGHT])   
    return edit_distances[-1][-1]
	
def optimal_search(model, request):
    try:
        category = str(request.GET['category'])
    except KeyError:
        category = ''
    try:
        desired_ingredients = request.GET['desired_ingredients']
    except KeyError:
        desired_ingredients = []
    else:
        desired_ingredients = [str(ingredient).strip().lower() for ingredient in desired_ingredients.split(',')]
        if desired_ingredients == ['']:
            desired_ingredients = []
    try:
        undesired_ingredients = request.GET['undesired_ingredients']
    except KeyError:
        undesired_ingredients = []
    else:
        undesired_ingredients = [str(ingredient).strip().lower() for ingredient in undesired_ingredients.split(',')]
        if undesired_ingredients == ['']:
            undesired_ingredients = []
    username = str(request.GET['username'])
    try:
        num_servings_make = int(request.GET['num_servings'])
    except (KeyError, ValueError):
        num_servings_make = float('inf')
    try:
        maximum_cost = int(request.GET['maximum_cost'])
    except (KeyError, ValueError):
        maximum_cost = float('inf')
    try:
        user_weights = request.GET['user_weights']
    except KeyError:
        user_weights = []
    else:
        user_weights = [int(weight) for weight in user_weights.split(',')]
        if user_weights == ['']:
            user_weights = []
    return optimization.optimize_search(model, category, desired_ingredients, undesired_ingredients, username, 
            num_servings_make, maximum_cost, user_weights)
            
counter = None
def insert(model, request):
	#recipeq = '(recipe_id, rating, name, category, cooktime, directions, link, image, number_of_servings,difficulty)'
	#pantryq = 'username'
	if model.__name__.lower() == 'recipe':
		querystring = recipe_query(model, request)
	if model.__name__.lower() == 'pantry':
		querystring = pantry_query(model, request)
        cursor = connection.cursor()
        cursor.execute(querystring)
	#return model.objects.raw(query)

pantryCounter = None
def recipe_query(model, request):
	global pantryCounter
	if not pantryCounter:
		#counterq = ' '.join(["SELECT MAX(recipe_id), recipe_id FROM", "search_" + model.__name__.lower(), ";"])
		pantryCounter = model.objects.all().aggregate(Max('recipe_id'))['recipe_id__max']
        if not pantryCounter:
            pantryCounter = 200
	pantryCounter += 1
	recipe_id = str(pantryCounter)
 	rating = str(request.POST['recipe_rating'])
	name = str(request.POST['recipe_name'])
	category = str(request.POST['recipe_category'])
	cooktime = str(request.POST['recipe_cooktime'])
	directions = str(request.POST['recipe_directions'])
	#link = str(request.POST['recipe_link'])
	link = 'stuff'
	#image = str(request.POST['recipe_image'])
	image = 'stuff'
	number_of_servings = str(request.POST['recipe_number_of_servings'])
	difficulty = str(request.POST['recipe_difficulty'])
	namestring = ''.join(["'", name, "'"])
	categorystring = ''.join(["'", category, "'"])
	directionsstring = ''.join(["'", directions, "'"])
	linkstring = ''.join(["'", link, "'"])
	imagestring = ''.join(["'", image, "'"])
	
	datastring = ','.join([recipe_id, rating, namestring, categorystring, cooktime, directionsstring, linkstring, imagestring, number_of_servings, difficulty])
	query = ' '.join(["INSERT INTO", "search_" + model.__name__.lower(), "(recipe_id, rating, name, category, cooktime, directions, link, image, number_of_servings,difficulty) VALUES", "(" + datastring + ");"])
	return query

def pantry_query(model, request):
    username = str(request.POST['pantry_username'])
    try:
        ingredients = str(request.POST['pantry_ingredients'])
    except KeyError:
        ingredients = []
    ingredient_names_amounts = [[string.strip() for string in ingredient.strip().split(':')] for ingredient in 
            ingredients.split(',') if ingredient]
    queries = []
    cursor = connection.cursor()
    have_username_query = ' '.join(["SELECT username FROM search_pantry WHERE username = ", "'" + username + "'"])
    cursor.execute(have_username_query)
    username_exists = cursor.fetchall()
    if username_exists and not ingredients:
        raise IntegrityError(username)
    elif username_exists:
        present_ingredients_query = ' '.join(["SELECT ingredient_name FROM search_contains WHERE username = ", 
            "'" + username.strip() + "'"])
        cursor.execute(present_ingredients_query)
        ingredient_names = cursor.fetchall()
    else:
        ingredient_names = []
        queries.append(' '.join(["INSERT INTO", "search_" + model.__name__.lower(), "(username) VALUES", 
            "(" + "'" +  username + "'" + ");"]))
    global counter
    if not counter:
            counter = models.Contains.objects.all().aggregate(Max('contains_id'))['contains_id__max']
    #Apparently ingredient_names as returned by cursor is a singleton tuple????
    ingredient_names = [name[0] for name in ingredient_names]
    for ingredient_name_amount in ingredient_names_amounts:
        ingredient_name, ingredient_amount = ingredient_name_amount
        ingredient_name = ingredient_name.strip()
        #assert False, "%s" % str(ingredient_name in ingredient_names)
        if ingredient_name in [name.strip() for name in ingredient_names]:
            query = ' '.join(["UPDATE search_contains", 
                              "SET quantity = ", str(ingredient_amount),
                              "WHERE ingredient_name = ", "'" + ingredient_name + "'", ';'])
        else:
            counter += 1
            query = ' '.join(["INSERT INTO", "search_contains", "(contains_id, username, ingredient_name, quantity)", 
                "VALUES", "(", ','.join([str(counter), "'" + username.lower().strip() + "'", "'" + 
                    ingredient_name.strip().lower() + "'", str(ingredient_amount)]), ")", ';'])
        queries.append(query)
    query = "UPDATE search_contains SET ingredient_name = replace(ingredient_name, '\n', '') WHERE ingredient_name LIKE '%%\n%%';"
    queries.append(query)
    return '\n'.join(queries)

def view_pantry(request):
    username = request.GET['view_pantry']
    query = ' '.join(['SELECT username, ingredient_name, quantity',
                        'FROM (search_pantry NATURAL JOIN search_contains)',
                        'WHERE username = ', "'" + username.lower() + "'"])
    cursor = connection.cursor()
    cursor.execute(query)
    results = optimization.dict_fetchall(cursor)
    if results:
        return [PantryData(result['username'], result['ingredient_name'], result['quantity']) for result in results]
    else:
        return None

class PantryData(object):

    def __init__(self, username, ingredient_name, quantity):
        self.username = username
        self.ingredient_name = ingredient_name
        self.quantity = quantity
    

	
def delete(model, request):
	q = request.POST['q']
	query = ' '.join(["DELETE FROM",  "search_" + model.__name__.lower(), "WHERE name =", "'" + q + "'", ";"])
	cursor = connection.cursor()
	cursor.execute(query)

def delete_pantry(model, request):
    q = request.POST['q']
    query = ' '.join(["DELETE FROM",  "search_" + model.__name__.lower(), "WHERE username =", "'" + q + "'", ";"])
    cursor = connection.cursor()
    cursor.execute(query)

def update_pantry(model, request):
	if 'username' in request.POST['username']:
		query = ' '.join(["UPDATE", "search_" + model.__name__.lower(), "SET username=", request.POST['username'], "WHERE contains_id =", id, ";"])
	if 'q' in request.POST['ingredient_name']:
		query = ' '.join(["UPDATE", "search_" + model.__name__.lower(), "SET ingredient_name=", request.POST['ingredient_name'], 
                    "WHERE contains_id =", id, ";"])
	if 'q' in request.POST['quantity']:
		query = ' '.join(["UPDATE", "search_" + model.__name__.lower(), "SET quantity=", request.POST['quantity'], "WHERE contains_id =", id, ";"])
