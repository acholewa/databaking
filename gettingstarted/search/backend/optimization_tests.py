from __future__ import division
import unittest
import optimization
import connection

class TestOptimization(unittest.TestCase):

    def  setUp(self):
        #expect to be able to make 25 servings.
        self.num_servings_make = 25
        self.maximum_cost = 200
        self.num_servings_recipe = 5
        self.amount_each_ingredient_pantry = [30, 40, 50]
        self.amount_each_ingredient_recipe = [1, 2, 3]
        self.cost_each_ingredient = [3, 2, 5]
        self.model = optimization.Recipe
            #optimization.Recipe({'recipe_id':123, 'rating':5, 'name':'chocolate cake', 'category':'cake', 'cooktime':120, 
            #'directions':'punch your face in the face', 'link':'there is no link', 'image':'there is no image', 'number_of_servings':5,
            #'difficulty':3})
        self.category = 'cake'
        self.desired_ingredients = ['chocolate']
        self.undesired_ingredients = ['nuts']
        self.username = 'andrew'
        self.user_weights = [1, 10, 20, 3]

    def test_optimize_search(self):
        result = optimization.optimize_search(self.model, self.category, self.desired_ingredients, self.undesired_ingredients, self.username,
                self.num_servings_make, self.maximum_cost) 
        #If we want to test user weights
        #result = optimization.optimize_search(self.model, self.category, self.desired_ingredients, self.undesired_ingredients, self.username,
                #self.maximum_cost, self.num_servings_make, self.user_weights) 
        expected_result = connection.Cursor.expected_result
        self.assertEqual([recipe.recipe_id for recipe in result], expected_result) 

    def test_enough_ingredients(self):
        recipe_scale = 5
        result = optimization.enough_ingredients(recipe_scale, self.amount_each_ingredient_pantry, 
                self.amount_each_ingredient_recipe)
        self.assertEqual(result, True)

    def test_enough_ingredients_scale_1(self):
        recipe_scale = 1
        result = optimization.enough_ingredients(recipe_scale, self.amount_each_ingredient_pantry, 
                self.amount_each_ingredient_recipe)
        self.assertEqual(result, True)

    def test_optimization(self):
        num_servings = optimization.optimize_recipe(self.num_servings_make, self.maximum_cost, self.num_servings_recipe, 
                self.amount_each_ingredient_pantry, self.amount_each_ingredient_recipe, self.cost_each_ingredient)
        self.assertEqual(num_servings, 25)


optimizationSuite = unittest.TestLoader().loadTestsFromTestCase(TestOptimization)
unittest.TextTestRunner(verbosity=2).run(optimizationSuite)



