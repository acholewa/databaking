from __future__ import division
import collections

"""
We have a few levels:
    1. Obtain all recipes in the desired recipe category.
    2. Narrow all such recipes to recipes that contain all ingredients with a weight of infinity (i.e. has to be there), and do not contain all ingredients to avoid with a weight of infinity 
    (i.e. absolutely cannot be there). Note that if a recipe has a substitution that adds an ingredient that has to be there, then we should also consider that new recipe. If a recipe has a 
    substitution that removes an ingredient that absolutely shouldn't be there then we include it.
    3. For each recipe:
        For each substitution:
            If the substitution is in Y, and the old ingredient is not in Y:
                replace the recipe with the new recipe where we include the substitution     
            elif the substitution is not in Z and the old ingredient is in Z:
                replace the recipe with the new recipe where we include the substitution
            elif the new is not in Y and the old is:
                ignore substitution
            elif the new is not in Z and the old is:
                ignore substitution
            otherwise:        
                generate two recipes: one with and one without the substitution
    4. For each recipe:
        value = a_1x_1 + Sigma_{i in use_ingredients union ingredients} a_i - Sigma_{i in avoid_ingredients intersect 
        ingredients} b_i  - a_4x_4
    return the 10 highest values.
"""

SERVINGS_WEIGHT = 0
DIFFICULTY_WEIGHT = -1

DEBUG = False
NUM_RESULTS = 1 if DEBUG else 10
if DEBUG:
    print("WARNING: Debugging is on. Using a mock connection.")
    #This is a dummy fake object that mimics the expected behavior of the actual connection object.
    from connection import connection
else:
    from django.db import connection


def optimize_search(model, category, desired_ingredients, undesired_ingredients, username, num_servings_make,
        maximum_cost, user_weights=None): 
    """
    Given a Django model, and a list of weights, obtains all recipes that could possibly be cooked. Then it weights
    the recipes based on the number of servings we can make, the number of desired ingredients it uses minus the 
    number of undesired ingredients it uses.
    model: The Django model of the table we're searching for recipes in.
    category: The recipe category we want.
    desired_ingredients: A list of strings representing ingredient names to include.
    undesired_ingredients: A list of strings representing ingredient names to avoid.
    username: The user whose pantry we want.
    user_weights: The weights the user is associating with each category.
    """
    desired_ingredients = [ingredient.lower().strip() for ingredient in desired_ingredients]
    undesired_ingredients = [ingredient.lower().strip() for ingredient in undesired_ingredients]
    if not user_weights:
        desired_ingredients_weights = [num_servings_make] * len(desired_ingredients)
        undesired_ingredients_weights = [-num_servings_make] * len(undesired_ingredients)
        user_weights = [1] + desired_ingredients_weights + undesired_ingredients_weights + [1]
    assert len(user_weights) == (2 + len(desired_ingredients) + len(undesired_ingredients)), "Incorrect number of weights: %s\n desired ingredients: %s\n undesired_ingredients: %s" % (str(user_weights), str(desired_ingredients), 
            str(undesired_ingredients))
    ingredient_weights = {}
    count = 1
    for ingredient in desired_ingredients:
        ingredient_weights[ingredient.strip()] = user_weights[count]
        count += 1
    for ingredient in undesired_ingredients:
        ingredient_weights[ingredient.strip()] = -user_weights[count]
        count += 1
    recipes_with_ingredients = ' '.join(['search_' + model.__name__.lower(), 'NATURAL JOIN search_uses'])
    recipes_with_ingredient_costs = ' '.join(['(', recipes_with_ingredients, ')', 'NATURAL JOIN search_ingredient'])
    valid_recipes = ' '.join(['SELECT recipe_id, ingredients, rating, name, category, cooktime, directions, link,', 'image,', 'number_of_servings, difficulty, ingredient_name, quantity, cost', 'FROM', '(', recipes_with_ingredient_costs, ') RecipeIngredients'])
    if category:
        valid_recipes = ' '.join([valid_recipes, 'WHERE category =', "'" + category.lower() + "'"])
    valid_recipes += ';'
    pantry = ' '.join(['SELECT ingredient_name, quantity',
                    'FROM (search_pantry NATURAL JOIN search_contains)',
                    'WHERE username =', "'" + username + "'", ';'])
    cursor = connection.cursor()                                        
    cursor.execute(valid_recipes)
    recipes = dict_fetchall(cursor)
    cursor.execute(pantry)
    pantry = dict_fetchall(cursor)
    pantry_amount = {row['ingredient_name'].strip():float(row['quantity']) for row in pantry}
    #Contains a mapping from a recipe id to a dict mapping ingredient_name to quantity.
    recipe_ingredient_dict = collections.defaultdict(dict)
    #Similar to above, except containing the cost of the recipe rather than the quantity.
    recipe_ingredient_cost = collections.defaultdict(dict)
    #Contains a mapping from the recipe id to dictionary of that recipe's information. 
    recipe_dict = {}
    for row in recipes:
        recipe_id = row['recipe_id']
        ingredient_name = row['ingredient_name'].strip()
        recipe_ingredient_dict[recipe_id][ingredient_name.strip()] = float(row['quantity'])
        recipe_ingredient_cost[recipe_id][ingredient_name.strip()] = float(row['cost'])
        recipe_dict[recipe_id] = row
    recipe_servings = {row['recipe_id']:row['number_of_servings'] for row in recipes}         
    #A set of pairs (recipe_id, weight). We're using a list of pairs rather than a dictionary because we want
    #to sort the list according to the weights,
    weights = set()
    for recipe_id, ingredient_map in recipe_ingredient_dict.iteritems():
       num_servings_recipe = recipe_servings[recipe_id]  
       pantryList = list(recipe_ingredient_dict[recipe_id])
       ingredient_list = recipe_ingredient_dict[recipe_id]
       cost_each_ingredient = []
       amount_each_ingredient_pantry = []
       amount_each_ingredient_recipe = []
       for ingredient_name in ingredient_list:
           try:
               amount_each_ingredient_pantry.append(pantry_amount[ingredient_name.strip()])
           except KeyError:
               amount_each_ingredient_pantry.append(0)
           amount_each_ingredient_recipe.append(ingredient_map[ingredient_name.strip()])
           cost_each_ingredient.append(recipe_ingredient_cost[recipe_id][ingredient_name.strip()]) 
       recipe_name = recipe_dict[recipe_id]['name']
       #RANDOM BULLSHIT
       num_servings = optimize_recipe(num_servings_make, maximum_cost, num_servings_recipe, amount_each_ingredient_pantry, 
               amount_each_ingredient_recipe, cost_each_ingredient, recipe_name)
       #We don't include recipes we can't make. Lies. We totally include recipes we can't make. Hopefully those are
       #weighted higher.
       if True:
           difficulty = -recipe_dict[recipe_id]['difficulty'] 
           weight = user_weights[SERVINGS_WEIGHT] * num_servings + user_weights[DIFFICULTY_WEIGHT] * difficulty
           ingredients_set = set(desired_ingredients + undesired_ingredients)
           recipe_ingredients_set = set(amount_each_ingredient_recipe)
           if num_servings:
               weight += sum([ingredient_weights[ingredient] for ingredient in ingredients_set & recipe_ingredients_set])
           weight -= num_servings_make * (len([pantryAm for pantryAm, recipeAm in zip(amount_each_ingredient_pantry,
                   amount_each_ingredient_recipe) if pantryAm < (num_servings_make / num_servings_recipe) * recipeAm]) + 1)
           WEIGHT = 1
           try:
               minWeight = min(weights, key=lambda x: x[WEIGHT])
           except ValueError:
               weights.add((recipe_id, weight))
           else:
               if minWeight[WEIGHT] < weight and len(weights) == NUM_RESULTS:
                   weights.remove(minWeight) 
                   weights.add((recipe_id, weight))
               elif len(weights) < NUM_RESULTS:
                   weights.add((recipe_id, weight))
       #else:
           #assert False
    weights = sorted(weights, key=lambda x:x[1], reverse=True)
    #assert False
    return [Recipe(recipe_dict[recipe_id]) for recipe_id, weight in weights]

def dict_fetchall(cursor):
    """Given a cursor, returns a list of rows. Each row is a dictionary mapping field names to the data stored in that row.
    This code was taken from: https://docs.djangoproject.com/en/1.7/topics/db/sql/#executing-custom-sql-directly"""
    desc = cursor.description
    return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
           ]

def optimize_recipe(num_servings_make, maximum_cost, num_servings_recipe, amount_each_ingredient_pantry, 
        amount_each_ingredient_recipe, cost_each_ingredient, recipe_name):
    """
    This function maximizes the number of servings. Let R be the set of ingredients in the current recipe, and let P be the 
    set of ingredients in the current
    pantry. Then, Given the following arguments:
    1. num_servings_make - how many servings the user would like to make.
    2. maximum_cost - how much the user is willing to spend
    3. num_servings_recipe - how many servings the recipe makes
    4. amount_each_ingredient_pantry - number of units of measurement of each ingredient in R intersect P
    5. amount_each_ingredient_recipe - number of units of measurement of each ingredient in R
    6. cost_each_ingredient - the cost of each ingredient in R intersect P

    Returns the maximum number of servings we can make (up to num_servings_make) within our costs using the ingredients 
    currently in our pantry.

    I was going to use linear programming, but that algorithm's complicated as shit. Since I'm only optimizing number of 
    servings, and I have an upper bound (num_servings_make), I'm just going to brute force it.
    pre: len(amount_each_ingredient_recipe) == len(amount_each_ingredient_pantry) == len(cost_each_ingredient)
    post: The number of servings returned is less than or equal to num_servings_make
    """
    assert len(amount_each_ingredient_recipe) == len(amount_each_ingredient_pantry) == len(cost_each_ingredient),\
        "One of the lists are not the correct length Lengths: %s, %s, %s" %\
        (str(len(amount_each_ingredient_recipe)), str(len(amount_each_ingredient_pantry)), str(len(cost_each_ingredient)))
    baseRecipeCost = sum([float(ingredient_cost) * float(amount_ingredient) for ingredient_cost, amount_ingredient in 
        zip(cost_each_ingredient, amount_each_ingredient_recipe)])
    max_servings_make = num_servings_make
    servings = num_servings_make
    if baseRecipeCost > maximum_cost or not enough_ingredients(1, amount_each_ingredient_pantry, 
            amount_each_ingredient_recipe):
        return 0
    while True:
        recipe_scale = (servings // num_servings_recipe)
        cost =  recipe_scale * baseRecipeCost
        sufficient_pantry = enough_ingredients(recipe_scale, amount_each_ingredient_pantry, amount_each_ingredient_recipe)
        if cost == maximum_cost and sufficient_pantry:
            return servings
        elif cost > maximum_cost or not sufficient_pantry:
            max_servings_make = servings
            servings = max_servings_make // 2
        elif cost < maximum_cost and sufficient_pantry: 
            jump = (max_servings_make - servings) // 2
            #If we're not careful, if we get down to max_servings_make = servings + 1, then we wind up in an infinite loop.
            if not jump:
                jump = max_servings_make - servings
            servings = servings + jump
            if servings == max_servings_make:
                return servings

def enough_ingredients(recipe_scale, amount_each_ingredient_pantry, amount_each_ingredient_recipe):
    """
    Given the arguments:
    1. recipe_scale - By how much we're scaling up the recipe (i.e. 2, 3, ... times)
    2. amount_each_ingredient_pantry - The amount of each ingredient in the recipe that we have in the pantry
    3. amount_each_ingredient_recipe - The amount of each ingredient that the recipe requires.

    Returns true if there are enough of every recipe in the pantry, returns false otherwise.
    This function will need to be modified when implementing fuzzy matching.
    """
    amount_each_ingredient = [recipe_scale * amount for amount in amount_each_ingredient_recipe]
    return all([recipe_amount <= pantry_amount for recipe_amount, pantry_amount in zip(amount_each_ingredient, 
        amount_each_ingredient_pantry)])


class Recipe(object):

    def __init__(self, recipe_information):
        self.recipe_id = recipe_information['recipe_id']
        self.rating = recipe_information['rating']
        self.name = recipe_information['name']
        self.category = recipe_information['category']
        self.cooktime = recipe_information['cooktime']
        self.directions = recipe_information['directions']
        self.link = recipe_information['link']
        self.image = recipe_information['image']
        self.number_of_servings = recipe_information['number_of_servings']
        self.difficulty = recipe_information['difficulty']
        self.ingredients = recipe_information['ingredients']

    def __str__(self):
        return self.name

