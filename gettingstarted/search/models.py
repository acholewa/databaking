from django.db import models

class Recipe(models.Model):
    recipe_id = models.SmallIntegerField(primary_key=True)
    rating = models.SmallIntegerField()
    name = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
    cooktime = models.SmallIntegerField()
    directions = models.TextField()
    link = models.URLField(max_length=200)
    image = models.URLField(max_length=200)
    number_of_servings = models.SmallIntegerField()
    difficulty = models.SmallIntegerField()
    ingredients = models.TextField()
    #ingredient = models.ForeignKey('Ingredient')


    def __str__(self):
        return '\n'.join(['recipe_id' + str(self.recipe_id), 'name: ' + str(self.name), 'category: ' + str(self.category),
            'rating: ' + str(self.rating), 'cooktime: ' + str(self.cooktime), 
            'number of servings: ' + str(self.number_of_servings), 'difficulty: ' + str(self.difficulty),
            'directions: ' + str(self.directions)])

class Uses(models.Model):
    uses_id = models.SmallIntegerField(primary_key=True)
    recipe_id = models.SmallIntegerField()
    ingredient_name = models.CharField(max_length=200)
    quantity = models.SmallIntegerField()

class Pantry(models.Model):
    username = models.CharField(max_length=200, primary_key=True)
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

class Contains(models.Model):
    contains_id = models.SmallIntegerField(max_length=200, primary_key=True)
    username = models.CharField(max_length=200)
    ingredient_name = models.CharField(max_length=200)
    quantity = models.SmallIntegerField(max_length=200)

class Ingredient(models.Model):
    name = models.CharField(max_length=200, primary_key=True)
    unit = models.CharField(max_length=200)
    cost = models.FloatField()

class SubstitutedWith(models.Model):
    substituted_with_id = models.SmallIntegerField(max_length=200,
            primary_key=True)
    original_ingredient = models.CharField(max_length=200)
    new_ingredient = models.CharField(max_length=200)
    original_amount = models.SmallIntegerField(max_length=200)
    new_amount = models.SmallIntegerField(max_length=200)

class Unit(models.Model):
    unit = models.CharField(max_length=200)

class Converts(models.Model):
    converts_id = models.SmallIntegerField(max_length=200,
            primary_key=True)
    original_unit = models.CharField(max_length=200)
    new_unit = models.CharField(max_length=200)
    conversion_rate_num = models.SmallIntegerField(max_length=200)
    conversion_rate_denom = models.SmallIntegerField(max_length=200)


