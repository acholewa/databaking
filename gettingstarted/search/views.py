# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.db import IntegrityError
from gettingstarted.search.models import Recipe
import gettingstarted.search.models
import parse 
import re

def search_form(request):
    return render(request, 'base.html')

def search(request):
    if 'q' in request.GET and request.GET['q']:
        recipes = parse.search(Recipe, request)
        allRecipeData = stringify_recipes(recipes)
        return HttpResponse('<br>------------------------<br>'.join(allRecipeData))

def stringify_recipes(recipes):
    allRecipeData = []
    for recipe in recipes:
        recipeData = '<br>'.join(['recipe_id: ' + str(recipe.recipe_id), 'name: ' + str(recipe.name), 
        'category: ' + str(recipe.category),
        'rating: ' + str(recipe.rating), 'cooktime: ' + str(recipe.cooktime) + " minutes", 
        'number of servings: ' + str(recipe.number_of_servings), 'difficulty: ' + str(recipe.difficulty),
        'ingredients: ' + str(recipe.ingredients),
        'directions: ' + str(recipe.directions)])
        allRecipeData.append(recipeData)
    return allRecipeData

def insert_pantry(request):
    if 'pantry_username' in request.POST and request.POST['pantry_username']:
        try:
            parse.insert(gettingstarted.search.models.Pantry, request)
        except IntegrityError, e:
            return HttpResponse(' '.join(["ERROR: username", request.POST['pantry_username'], "already exists."]))
        return HttpResponse('updated')
    return HttpResponse('Did not include a username')

def view_pantry(request):
    if 'view_pantry' in request.GET and request.GET['view_pantry']:
        results = parse.view_pantry(request)
        if results:
            username = results[0].username
            resultStrings = [' '.join(["Pantry for:", username])]
            resultStrings += [':'.join([result.ingredient_name, str(result.quantity)]) for result in results]
            return HttpResponse('</br><br>'.join(resultStrings) + '</br>')
        else:
            return HttpResponse(' '.join(["Either username:", request.GET['view_pantry'], "is not valid username, or",
                "your pantry is empty."]))
    else:
        return HttpResponse("Must provide a username.")

#def update_pantry(request):

def fancy_search(request):
    if 'username' in request.GET and request.GET['username']:
        #recipes = parse.optimal_search(Recipe, request)
        recipes = parse.optimal_search(Recipe, request)
        if recipes:
            allRecipeData = stringify_recipes(recipes)
            return HttpResponse('<br>------------------------<br>'.join(allRecipeData))
        else:
            return HttpResponse("You don't have enough ingredients to make any of our recipes, or you're not willing to spend enough to make even one serving of any of our recipes!")
    else:
        return HttpResponse('<br>Did not provide a username.<br>')

def insert(request):
    if request.method == 'POST':
        parse.insert(Recipe, request)
    return HttpResponse('inserted')

def delete(request):
    if request.method == 'POST':
        parse.delete(Recipe, request)
    return HttpResponse('deleted')

def delete_pantry(request):
    if request.method == 'POST':
        parse.delete_pantry(gettingstarted.search.models.Pantry, request)
        parse.delete_pantry(gettingstarted.search.models.Contains, request)
    return HttpResponse('deleted')
