from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()
import views
from gettingstarted.search import views

urlpatterns = patterns('',
    url(r'^$', 'gettingstarted.views.home', name='home'),
    url(r'^search/$', views.search),
    url(r'^insert/$', views.insert),
    url(r'^delete/$', views.delete),
    url(r'^view_pantry/$', views.view_pantry),
    url(r'^insert_pantry/$', views.insert_pantry),
    url(r'^fancy_search/$', views.fancy_search),
    url(r'^delete_pantry/$', views.delete_pantry),
    
    url(r'^admin/', include(admin.site.urls)),

)
