from django.db import connection
"""
This module is responsible for the commands that actually fetch data from the database. The primary method is get_data(query).
get_data takes a raw SQL query, and returns a dictionary object mapping the field names to their data.
"""

def get_data(query):
    """
    Given a raw SQL query, returns a list of dictionaries. Each dictionary contains the data of a single row in the result, 
    with
    each field name is mapped to the associated data.
    See: https://docs.djangoproject.com/en/1.7/topics/db/sql/#executing-custom-sql-directly
    """
    cursor = connection.cursor()
    cursor.execute(query)
    results = dict_fetchall(cursor)
    return results

def dict_fetchall(cursor):
    """Given a cursor, returns a list of rows. Each row is a dictionary mapping field names to the data stored in that row.
    This code was taken from: https://docs.djangoproject.com/en/1.7/topics/db/sql/#executing-custom-sql-directly"""
    desc = cursor.description
    return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
           ]
